# Changelog

## v1.0.2
### Changed
  * CHANGELOG.md removal of dates

## v1.0.1
### Changed
  * README.md instruction updates

## v1.0.0
- Initial Release